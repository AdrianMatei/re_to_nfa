#include "NFA.h"
#include "DFA.h"

bool NFA::addState(std::string state)
{
	for (int index = 0; index < Q.size(); index++)
		if (Q[index] == state)
			return 0;
	this->Q.push_back(state);
	return 1;
}

void NFA::addAlphabet(std::string alphabet)
{
	this->Sigma = alphabet;
}

void NFA::addTransition(NFA_transition t)
{
	this->Delta.push_back(t);
}

void NFA::addInitialState(std::string state)
{
	this->q0 = state;
}

void NFA::addFinalState(std::string state)
{
	this->F.push_back(state);
}

std::vector<std::string> NFA::getQ()
{
	return this->Q;
}

std::string NFA::getSigma()
{
	return this->Sigma;
}

void NFA::printNFA()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	system("chcp 1253");
	system("CLS");

	SetConsoleTextAttribute(hConsole, yellow);
	std::cout << "\n NFA:\n";

	SetConsoleTextAttribute(hConsole, blue);
	std::cout << "\n Q = {\n";
	for (int index = 0; index < Q.size(); index++)
	{
		std::cout << "         " << Q[index];
		if (index != Q.size() - 1)
			std::cout << "\n";
	}
	std::cout << "\n }\n\n ";
	Sleep(T);
	std::cout << (char)211 << " = {";
	for (int index = 0; index < Sigma.length(); index++)
	{
		std::cout << Sigma[index];
		if (index < Sigma.length() - 1)
			std::cout << ", ";
	}
	std::cout << '}';
	Sleep(T);
	if (Delta.size() > 0)
	{
		std::cout << "\n\n " << (char)228 << ": " << Delta[0].state_1 << " -(" << Delta[0].symbol << ")-> " << Delta[0].state_2;
		Sleep(T);
		for (int index = 1; index < Delta.size(); index++)
		{
			std::cout << "\n    " << Delta[index].state_1 << " -(" << Delta[index].symbol << ")-> " << Delta[index].state_2;
			Sleep(T);
		}
	}
	std::cout << "\n\n q0 = " << q0;
	Sleep(T);
	std::cout << "\n\n F = {\n";
	for (int index = 0; index < F.size(); index++)
	{
		std::cout << "         " << F[index];
		if (index != F.size() - 1)
			std::cout << "\n";
	}
	std::cout << "\n }\n\n";
	Sleep(T);
	SetConsoleTextAttribute(hConsole, white);
}

void NFA::printNFA2()
{
	for (int index = 0; index < Q.size(); index++)
	{
		std::cout << Q[index];
		if (index != Q.size() - 1)
			std::cout << " ";
	}

	std::cout << "\n";
	for (int index = 0; index < Sigma.length(); index++)
	{
		std::cout << Sigma[index];
		if (index < Sigma.length() - 1)
			std::cout << " ";
	}

	if (Delta.size() > 0)
	{

		for (int index = 0; index < Delta.size(); index++)
			std::cout << "\n" << Delta[index].state_1 << " " << Delta[index].symbol << " " << Delta[index].state_2;
	}

	std::cout << "\n" << q0;

	std::cout << "\n";
	for (int index = 0; index < F.size(); index++)
	{
		std::cout << F[index];
		if (index != F.size() - 1)
			std::cout << " ";
	}
}

void NFA::renameStates()
{
	std::vector<std::pair<std::string, std::string>> states;
	std::pair<std::string, std::string> newPair;
	int c = 0;

	for (int state = 0; state < Q.size(); state++)
	{
		newPair.first = "p" + std::to_string(c);
		newPair.second = Q[state];
		states.push_back(newPair);
		Q[state] = newPair.first;
		c++;
	}

	for (int transition = 0; transition < Delta.size(); transition++)
	{
		for (int state = 0; state < states.size(); state++)
		{
			if (Delta[transition].state_1 == states[state].second)
				Delta[transition].state_1 = states[state].first;

			if (Delta[transition].state_2 == states[state].second)
				Delta[transition].state_2 = states[state].first;
		}
	}

	for (int state = 0; state < states.size(); state++)
		if (states[state].second == q0)
			q0 = states[state].first;

	for (int finalState = 0; finalState < F.size(); finalState++)
	{
		for (int state = 0; state < states.size(); state++)
		{
			if (F[finalState] == states[state].second)
				F[finalState] = states[state].first;
		}
	}
}

DFA NFA::NFA_to_DFA()
{
	system("pause");
	system("CLS");

	std::vector<std::vector<std::string>> states;

	std::vector<std::string> currentState;
	std::vector<std::string> newState;

	std::vector<DFA_transition> newTransitions;

	std::vector<std::string> F2;

	newState.push_back(q0);
	states.push_back(newState);

	for (int index = 0; index < states.size(); index++)
	{
		currentState = states[index];

		for (int letter = 0; letter < Sigma.size(); letter++)
		{
			for (int state = 0; state < currentState.size(); state++)
			{
				for (int transition = 0; transition < Delta.size(); transition++)
				{
					if (Delta[transition].state_1 == currentState[state] && Delta[transition].symbol == Sigma[letter])
					{
						for (int state2 = 0; state2 < Delta[transition].state_2.size(); state2++)
							newState.push_back(Delta[transition].state_2);
					}
				}
			}

			if (newState.size() > 1)
			{
				for (int i = 0; i < newState.size() - 1; i++)
				{
					for (int j = i + 1; j < newState.size(); j++)
					{
						if (newState[i] == newState[j])
						{
							newState.erase(newState.begin() + j);
							i = 0;
						}
					}
				}

				quickSort(newState, 0, newState.size() - 1);
			}

			bool OK = 1;

			for (int state = 0; state < states.size(); state++)
			{
				if (states[state] == newState)
					OK = 0;
			}

			if (OK == 1 && newState.size() > 0)
			{
				states.push_back(newState);

				bool isFinalState = 0;

				for (int i = 0; i < newState.size(); i++)
				{
					for (int finalState = 0; finalState < F.size(); finalState++)
					{
						if (newState[i] == F[finalState])
							isFinalState = 1;
					}
				}

				if (isFinalState == 1)
					F2.push_back("p" + std::to_string(findIndex(states, newState)));

			}

			if (findIndex(states, currentState) != -1 && findIndex(states, newState) != -1)
			{
				DFA_transition t;
				t.state_1 = "p" + std::to_string(findIndex(states, currentState));
				t.symbol = Sigma[letter];
				t.state_2 = "p" + std::to_string(findIndex(states, newState));
				newTransitions.push_back(t);
			}

			newState.clear();

		}
	}

	DFA M;

	std::vector<std::string> Q2;

	std::string p = "p";
	int c = 0;

	std::cout << "\n\n\n";

	for (int state = 0; state < states.size(); state++)
	{
		std::string str = p;
		Q2.push_back(str.append(std::to_string(c)));
		c++;
	}

	M.setQ(Q2);

	M.setSigma(Sigma);

	M.setDelta(newTransitions);

	M.setQ0("p" + std::to_string(findIndex(states, states[0])));

	M.setF(F2);

	M.printDFA();

	return M;

}