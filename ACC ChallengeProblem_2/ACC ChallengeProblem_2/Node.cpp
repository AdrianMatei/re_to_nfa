#include "Node.h"

Node* Node::newNode(int value)
{
    Node* newNode = new Node;

    newNode->value = value;
    newNode->left = NULL;
    newNode->right = NULL;

    return newNode;
}

char Node::getValue()
{
    return this->value;
}

Node* Node::getLeftChild()
{
    return this->left;
}

Node* Node::getRightChild()
{
    return this->right;
}

void Node::setLeftChild(Node* child)
{
    this->left = child;
}

void Node::setRightChild(Node* child)
{
    this->right = child;
}

std::vector<int> Node::getFirstPos()
{
    return this->firstpos;
}

std::vector<int> Node::getLastPos()
{
    return this->lastpos;
}

std::vector<int> Node::getFollowPos()
{
    return this->followpos;
}

void Node::addToFirstPos(int value)
{
    this->firstpos.push_back(value);
}

void Node::addToLastPos(int value)
{
    this->lastpos.push_back(value);
}

void Node::addToFollowPos(int value)
{
    this->followpos.push_back(value);
}

bool Node::getNullable()
{
    return this->isNullable;
}

void Node::setNullable(bool isNullable)
{
    this->isNullable = isNullable;
}

void Node::printNodeInformation()
{
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

    SetConsoleTextAttribute(hConsole, green);

    std::cout << " {";
    for (int index = 0; index < this->firstpos.size(); index++)
    {
        std::cout << this->firstpos[index];
        if (index + 1 < this->firstpos.size())
            std::cout << ", ";
    }
    std::cout << "}";

    SetConsoleTextAttribute(hConsole, red);

    std::cout << " {";
    for (int index = 0; index < this->lastpos.size(); index++)
    {
        std::cout << this->lastpos[index];
        if (index + 1 < this->lastpos.size())
            std::cout << ", ";
    }
    std::cout << "}";

    if (this->getLeftChild() == NULL && this->getRightChild() == NULL)
    {
        SetConsoleTextAttribute(hConsole, blue);

        std::cout << " {";
        for (int index = 0; index < this->followpos.size(); index++)
        {
            std::cout << this->followpos[index];
            if (index + 1 < this->followpos.size())
                std::cout << ", ";
        }
        std::cout << "}";
    }

    SetConsoleTextAttribute(hConsole, white);
}

