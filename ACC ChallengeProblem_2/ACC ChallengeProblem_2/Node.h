#pragma once
#include <iostream>
#include <vector>
#include <windows.h>

#define green 10
#define blue 11
#define red 12
#define yellow 14
#define white 15

class Node
{
private:
	char value;
	Node* left;
	Node* right;

	bool isNullable;

	std::vector<int> firstpos;
	std::vector<int> lastpos;
	std::vector<int> followpos;

public:
	Node* newNode(int value);

	char getValue();

	Node* getLeftChild();
	Node* getRightChild();

	void setLeftChild(Node* child);
	void setRightChild(Node* child);

	std::vector<int> getFirstPos();
	std::vector<int> getLastPos();
	std::vector<int> getFollowPos();

	void addToFirstPos(int value);
	void addToLastPos(int value);
	void addToFollowPos(int value);

	bool getNullable();
	void setNullable(bool isNullable);

	void printNodeInformation();
};

