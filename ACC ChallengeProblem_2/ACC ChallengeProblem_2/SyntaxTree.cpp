#include "SyntaxTree.h"

Node* SyntaxTree::constructTree(const std::string& expression)
{
	std::stack<Node*> st;

	Node* root = NULL;
	Node* newNode = NULL;
	Node* leftChild;
	Node* rightChild;

	int pos = 1;

	for (int index = 0; index < expression.length(); index++)
	{
		if (!isOperator(expression[index]))
		{
			if (expression[index] != '#')
			{
				newNode = newNode->newNode(expression[index]);
				newNode->addToFirstPos(pos);
				newNode->addToLastPos(pos);
				pos++;
				st.push(newNode);
			}
			else
			{
				newNode = newNode->newNode(expression[index]);

				root = root->newNode('.');
				root->setLeftChild(st.top());
				root->setRightChild(newNode);

				newNode->addToFirstPos(pos);
				newNode->addToLastPos(pos);

				st.pop();
				st.push(root);

				this->root = root;
			}
		}
		else
		{
			newNode = newNode->newNode(expression[index]);

			if (expression[index] == '*')
			{
				leftChild = st.top();
				st.pop();
				newNode->setLeftChild(leftChild);
			}
			else
			{
				rightChild = st.top();
				st.pop();
				leftChild = st.top();
				st.pop();

				newNode->setLeftChild(leftChild);
				newNode->setRightChild(rightChild);
			}

			st.push(newNode);
		}
	}

	newNode = st.top();
	st.pop();

	return newNode;
}

void SyntaxTree::printTree(Node* node)
{
	std::cout << "\n\n";
	printBT("", node, false);
}

Node* SyntaxTree::getRoot()
{
	return this->root;
}

void SyntaxTree::setNodesNullable(Node* node)
{
	if (node == NULL)
		return;

	setNodesNullable(node->getLeftChild());
	setNodesNullable(node->getRightChild());

	if (node->getLeftChild() == NULL && node->getRightChild() == NULL)
		node->setNullable(0);
	else
		if (node->getValue() == '+')
		{
			if (node->getLeftChild()->getNullable() == 1 ||
				node->getRightChild()->getNullable() == 1)
				node->setNullable(1);
			else
				node->setNullable(0);
		}
		else
			if (node->getValue() == '.')
			{
				if (node->getLeftChild()->getNullable() == 1 &&
					node->getRightChild()->getNullable() == 1)
					node->setNullable(1);
				else
					node->setNullable(0);
			}
			else
				if (node->getValue() == '*')
					node->setNullable(1);
}

void SyntaxTree::setNodesFirstPosAndLastPos(Node* node)
{
	if (node == NULL)
		return;

	setNodesFirstPosAndLastPos(node->getLeftChild());
	setNodesFirstPosAndLastPos(node->getRightChild());

	Node* c1 = node->getLeftChild();
	Node* c2 = node->getRightChild();

	switch (node->getValue())
	{
	case '+':

		for (int index = 0; index < c1->getFirstPos().size(); index++)
			node->addToFirstPos(c1->getFirstPos()[index]);

		for (int index = 0; index < c2->getFirstPos().size(); index++)
			node->addToFirstPos(c2->getFirstPos()[index]);

		for (int index = 0; index < c1->getLastPos().size(); index++)
			node->addToLastPos(c1->getLastPos()[index]);

		for (int index = 0; index < c2->getLastPos().size(); index++)
			node->addToLastPos(c2->getLastPos()[index]);

		break;



	case '.':

		if (c1->getNullable() == 1)
		{
			for (int index = 0; index < c1->getFirstPos().size(); index++)
				node->addToFirstPos(c1->getFirstPos()[index]);

			for (int index = 0; index < c2->getFirstPos().size(); index++)
				node->addToFirstPos(c2->getFirstPos()[index]);
		}
		else
			for (int index = 0; index < c1->getFirstPos().size(); index++)
				node->addToFirstPos(c1->getFirstPos()[index]);

		if (c2->getNullable() == 1)
		{
			for (int index = 0; index < c1->getLastPos().size(); index++)
				node->addToLastPos(c1->getLastPos()[index]);

			for (int index = 0; index < c2->getLastPos().size(); index++)
				node->addToLastPos(c2->getLastPos()[index]);
		}
		else
			for (int index = 0; index < c2->getLastPos().size(); index++)
				node->addToLastPos(c2->getLastPos()[index]);

		break;



	case '*':
		for (int index = 0; index < c1->getFirstPos().size(); index++)
			node->addToFirstPos(c1->getFirstPos()[index]);

		for (int index = 0; index < c1->getLastPos().size(); index++)
			node->addToLastPos(c1->getLastPos()[index]);

		break;
	}
}

Node* SyntaxTree::findNodeByIndex(Node* node, int index)
{
	if (node != NULL)
	{
		if (node->getLeftChild() == NULL && node->getRightChild() == NULL &&
			node->getFirstPos()[0] == node->getLastPos()[0] && node->getFirstPos()[0] == index)
		{
			return node;
		}
		else
		{
			Node* foundNode = findNodeByIndex(node->getLeftChild(), index);
			if (foundNode == NULL)
				foundNode = findNodeByIndex(node->getRightChild(), index);
			return foundNode;
		}
	}
	else
	{
		return NULL;
	}
}

void SyntaxTree::setFollowPos(Node* node)
{
	if (node == NULL)
		return;

	setFollowPos(node->getLeftChild());
	setFollowPos(node->getRightChild());

	if (node->getValue() == '*')
	{
		for (int index = 0; index < node->getLastPos().size(); index++)
		{
			Node* n = findNodeByIndex(this->root, node->getLastPos()[index]);
			if (n != NULL)
			{
				for (int index_2 = 0; index_2 < node->getFirstPos().size(); index_2++)
					n->addToFollowPos(node->getFirstPos()[index_2]);
			}
		}
	}

	if (node->getValue() == '.')
	{
		for (int index = 0; index < node->getLeftChild()->getLastPos().size(); index++)
		{
			Node* n = findNodeByIndex(this->root, node->getLeftChild()->getLastPos()[index]);
			if (n != NULL)
			{
				for (int index_2 = 0; index_2 < node->getRightChild()->getFirstPos().size(); index_2++)
					n->addToFollowPos(node->getRightChild()->getFirstPos()[index_2]);
			}
		}
	}

}

void SyntaxTree::createNFA(NFA& M, std::string regularExpression)
{
	M.addAlphabet(getAlphabet(regularExpression));

	std::vector<int> firstNode = root->getFirstPos();

	quickSort(firstNode, 0, firstNode.size() - 1);

	M.addState(vectorToString(firstNode));

	M.addInitialState(vectorToString(firstNode));

	std::queue<std::vector<int>> states;
	states.push(firstNode);

	std::string alphabet = M.getSigma();
	std::vector<std::vector<int>> newStates;

	for (int index = 0; index < alphabet.size(); index++)
	{
		std::vector<int> v;
		newStates.push_back(v);
	}

	while (!states.empty())
	{
		std::vector<int> currentState = states.front();

		states.pop();

		for (int letter = 0; letter < alphabet.size(); letter++)
		{
			std::vector<int> v;
			newStates[letter].clear();

			for (int index = 0; index < currentState.size(); index++)
			{
				Node* n = findNodeByIndex(root, currentState[index]);

				if (n->getValue() == alphabet[letter])
				{
					v = n->getFollowPos();
					addFollowPos(v, newStates[letter]);
				}
			}

			for (int index = 0; index < newStates.size(); index++)
				if (newStates[index].size() > 0)
					quickSort(newStates[index], 0, newStates[index].size() - 1);

			NFA_transition t;

			t.state_1 = vectorToString(currentState);
			t.symbol = alphabet[letter];
			t.state_2 = vectorToString(newStates[letter]);

			if (t.state_1.length() > 2 && t.state_2.length() > 2)
				M.addTransition(t);

			v.clear();
		}

		for (int index = 0; index < newStates.size(); index++)
		{
			if (newStates[index].size() > 0)
			{
				std::string newState = vectorToString(newStates[index]);

				if (M.addState(newState) == 1)
				{
					states.push(newStates[index]);
					for (int i = 0; i < newStates[index].size(); i++)
						if (findNodeByIndex(root, newStates[index][i])->getValue() == '#')
							M.addFinalState(vectorToString(newStates[index]));
				}
			}
		}
	}
}
