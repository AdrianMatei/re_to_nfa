#pragma once

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <windows.h>
#include "Utils.h"

#define T 0 //50
#define green 10
#define blue 11
#define red 12
#define yellow 14
#define white 15

struct DFA_transition {
	std::string state_1;
	char symbol;
	std::string state_2;
};

class DFA
{
private:
	std::vector<std::string> Q;
	std::string Sigma;
	std::vector<DFA_transition> Delta;
	std::string q0;
	std::vector<std::string> F;

public:
	void printDFA();
	void printDFA2();

	bool addState(std::string state);
	void addAlphabet(std::string alphabet);
	void addTransition(DFA_transition t);
	void addInitialState(std::string state);
	void addFinalState(std::string state);

	void setQ(std::vector<std::string> stari);
	void setSigma(std::string alfabet);
	void setDelta(std::vector<DFA_transition> reguli);
	void setQ0(std::string stareInitiala);
	void setF(std::vector<std::string> stariFinale);

	void minimizeDFA();
	bool marcare(std::vector<std::vector<int>>& matrice, int i, int j, std::vector<std::string> Q, std::string Sigma, std::vector<DFA_transition> Delta);
};

