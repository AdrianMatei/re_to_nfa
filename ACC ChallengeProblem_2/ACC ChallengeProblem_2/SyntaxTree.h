#pragma once
#include <iostream>
#include <stack>
#include <queue>
#include <string>
#include "Node.h"
#include "NFA.h"
#include "Utils.h"

class SyntaxTree
{
private:
	Node* root;

public:
	Node* constructTree(const std::string& expression);
	void printTree(Node* node);
	Node* getRoot();
	void setNodesNullable(Node* node);
	void setNodesFirstPosAndLastPos(Node* node);
	Node* findNodeByIndex(Node* node, int index);
	void setFollowPos(Node* node);
	void createNFA(NFA& M, std::string regularExpression);
};

