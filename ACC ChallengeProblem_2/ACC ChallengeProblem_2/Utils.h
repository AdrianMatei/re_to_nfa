#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <stack>
#include "Node.h"
#include "DFA.h"

std::string getRegularExpression();
std::string convertToAugmentedRegularExpression(std::string regularExpression);
int precedence(char c);
std::string infixToPostfix(std::string infixExpression);

bool isOperator(char c);
std::string getAlphabet(std::string regularExpression);
std::string vectorToString(std::vector<int> v);
void addFollowPos(std::vector<int> v, std::vector<int>& newStates);
void quickSort(std::vector<int>& v, int st, int dr);
void quickSort(std::vector<std::string>& v, int st, int dr);
void printBT(const std::string& prefix, Node* node, bool isLeft);
int findIndex(std::vector<std::vector<std::string>> states, std::vector<std::string> newState);