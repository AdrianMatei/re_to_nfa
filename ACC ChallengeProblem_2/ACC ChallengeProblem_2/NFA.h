#pragma once

#include<iostream>
#include<vector>
#include<string>
#include<fstream>
#include<windows.h>
#include "Utils.h"

#define T 0 //50
#define green 10
#define blue 11
#define red 12
#define yellow 14
#define white 15

struct NFA_transition {
	std::string state_1;
	char symbol;
	std::string state_2;
};

class NFA
{
private:
	std::vector<std::string> Q;
	std::string Sigma;
	std::vector<NFA_transition> Delta;
	std::string q0;
	std::vector<std::string> F;

public:
	void printNFA();
	void printNFA2();

	void renameStates();

	bool addState(std::string state);
	void addAlphabet(std::string alphabet);
	void addTransition(NFA_transition t);
	void addInitialState(std::string state);
	void addFinalState(std::string state);

	std::vector<std::string> getQ();
	std::string getSigma();

	DFA NFA_to_DFA();
};

