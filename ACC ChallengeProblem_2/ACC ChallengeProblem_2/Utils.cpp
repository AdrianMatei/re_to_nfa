#include "Utils.h"

std::string getRegularExpression()
{
	std::ifstream fin("regularExpression_5.txt");

	std::string regularExpression;
	std::getline(fin, regularExpression);

	fin.close();

	return regularExpression;
}

std::string convertToAugmentedRegularExpression(std::string regularExpression)
{
	std::string augmentedRegularExpression;
	char c, c2;

	for (int index = 0; index < regularExpression.size(); index++)
	{
		c = regularExpression[index];

		if (index < regularExpression.size() - 1)
		{
			c2 = regularExpression[index + 1];
			augmentedRegularExpression += c;
			if (c != '(' && c2 != ')' && c != '+' && c2 != '+' && c2 != '*') {
				augmentedRegularExpression += '.';
			}
		}
	}

	augmentedRegularExpression += regularExpression[regularExpression.size() - 1];

	augmentedRegularExpression += '#';

	return augmentedRegularExpression;
}

int precedence(char c)
{
	if (c == '*')
		return 3;
	else if (c == '.')
		return 2;
	else if (c == '+')
		return 1;
	else
		return -1;
}

std::string infixToPostfix(std::string infixExpression)
{
	std::stack<char> st;
	st.push('~');

	std::string postfixExpression;

	for (int index = 0; index < infixExpression.length(); index++)
	{
		if ((infixExpression[index] >= 'a' && infixExpression[index] <= 'z') ||
			(infixExpression[index] >= 'A' && infixExpression[index] <= 'Z'))
			postfixExpression += infixExpression[index];
		else
			if (infixExpression[index] == '(')
				st.push('(');
			else
				if (infixExpression[index] == ')')
				{
					while (st.top() != '~' && st.top() != '(')
					{
						char c = st.top();
						st.pop();
						postfixExpression += c;
					}
					if (st.top() == '(')
					{
						char c = st.top();
						st.pop();
					}
				}
				else
				{
					while (st.top() != '~' &&
						precedence(infixExpression[index]) <= precedence(st.top()))
					{
						char c = st.top();
						st.pop();
						postfixExpression += c;
					}
					st.push(infixExpression[index]);
				}

	}

	while (st.top() != '~')
	{
		char c = st.top();
		st.pop();
		postfixExpression += c;
	}

	return postfixExpression;
}

bool isOperator(char c)
{
	if (c == '.' || c == '+' || c == '*')
		return true;
	return false;
}

std::string getAlphabet(std::string regularExpression)
{
	std::string alphabet = "";

	for (int index = 0; index < regularExpression.length(); index++)
		if (regularExpression[index] != '(' &&
			regularExpression[index] != ')' &&
			regularExpression[index] != '#' &&
			regularExpression[index] != '.' &&
			regularExpression[index] != '*' &&
			regularExpression[index] != '+')
			alphabet = alphabet + regularExpression[index];

	for (int i = 0; i < alphabet.length(); i++)
		for (int j = i + 1; j < alphabet.length(); j++)
			if (alphabet[i] == alphabet[j])
			{
				alphabet.erase(alphabet.begin() + j);
				j--;
			}

	return alphabet;
}

std::string vectorToString(std::vector<int> v)
{
	std::string string;

	string = string + '{';

	for (int index = 0; index < v.size(); index++)
	{
		string = string + std::to_string(v[index]);
		if (index + 1 < v.size())
			string = string + ", ";
	}

	string = string + '}';

	return string;
}

void addFollowPos(std::vector<int> v, std::vector<int>& newStates)
{
	for (int i = 0; i < v.size(); i++)
	{
		bool found = 0;
		for (int j = 0; j < newStates.size(); j++)
			if (newStates[j] == v[i])
				found = 1;
		if (found == 0)
			newStates.push_back(v[i]);
	}
}

void quickSort(std::vector<int>& v, int st, int dr)
{
	int p, i, j, aux;

	i = st;
	j = dr;
	p = v[(st + dr) / 2];

	do {

		while ((i < dr && v[i] < p))
			i++;

		while ((j > st&& v[j] > p))
			j--;

		if (i <= j)
		{
			aux = v[i];
			v[i] = v[j];
			v[j] = aux;
			i++;
			j--;
		}

	} while (i <= j);

	if (st < j)
		quickSort(v, st, j);

	if (i < dr)
		quickSort(v, i, dr);
}

void quickSort(std::vector<std::string>& v, int st, int dr)
{
	int i, j;
	std::string p, aux;

	i = st;
	j = dr;
	p = v[(st + dr) / 2];

	do {

		while ((i < dr && v[i] < p))
			i++;

		while ((j > st&& v[j] > p))
			j--;

		if (i <= j)
		{
			aux = v[i];
			v[i] = v[j];
			v[j] = aux;
			i++;
			j--;
		}

	} while (i <= j);

	if (st < j)
		quickSort(v, st, j);

	if (i < dr)
		quickSort(v, i, dr);
}

void printBT(const std::string& prefix, Node* node, bool isLeft)
{
	if (node != nullptr)
	{
		std::cout << prefix;

		if (isLeft == 1)
			std::cout << "|--";
		else
			std::cout << "L__";

		std::cout << node->getValue();
		node->printNodeInformation();
		std::cout << std::endl;

		if (isLeft == 1)
			printBT(prefix + "|   ", node->getLeftChild(), true);
		else
			printBT(prefix + "    ", node->getLeftChild(), true);

		if (isLeft == 1)
			printBT(prefix + "|   ", node->getRightChild(), false);
		else
			printBT(prefix + "    ", node->getRightChild(), false);

	}
	else
	{
		std::cout << prefix;

		if (isLeft == 1)
			std::cout << "|--";
		else
			std::cout << "L__";

		std::cout << "null" << std::endl;
	}
}

int findIndex(std::vector<std::vector<std::string>> states, std::vector<std::string> newState)
{
	for (int index = 0; index < states.size(); index++)
	{
		if (states[index] == newState)
			return index;
	}
	return -1;
}
