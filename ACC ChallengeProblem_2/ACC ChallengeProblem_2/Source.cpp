#include <iostream>
#include "NFA.h"
#include "SyntaxTree.h"
#include "Utils.h"

int main()
{
	std::string regularExpression = getRegularExpression();
	std::cout << "\n Regular Expression:                     " << regularExpression;

	regularExpression = convertToAugmentedRegularExpression(regularExpression);
	std::cout << "\n Augmented Regular Expression:           " << regularExpression;

	regularExpression = infixToPostfix(regularExpression);
	std::cout << "\n Postfix Augmented Regular Expression:   " << regularExpression;

	SyntaxTree Tree;
	Tree.constructTree(regularExpression);
	Tree.printTree(Tree.getRoot());
	Tree.setNodesNullable(Tree.getRoot());
	Tree.setNodesFirstPosAndLastPos(Tree.getRoot());
	Tree.printTree(Tree.getRoot());
	Tree.setFollowPos(Tree.getRoot());
	Tree.printTree(Tree.getRoot());

	system("pause");

	NFA M;
	Tree.createNFA(M, regularExpression);

	M.printNFA();
	M.renameStates();
	M.printNFA2();

	/*DFA N = M.NFA_to_DFA();
	N.minimizeDFA();
	N.printDFA();*/

	std::cout << "\n\n\n";
	//system("pause");
	return 0;
}