#include "DFA.h"
#include "Utils.h"

void DFA::printDFA()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
	system("chcp 1253");
	system("CLS");

	SetConsoleTextAttribute(hConsole, yellow);
	std::cout << "\n DFA:\n";

	SetConsoleTextAttribute(hConsole, blue);
	std::cout << "\n Q = {\n";
	for (int index = 0; index < Q.size(); index++)
	{
		std::cout << "         " << Q[index];
		if (index != Q.size() - 1)
			std::cout << "\n";
	}
	std::cout << "\n }\n\n ";
	Sleep(T);
	std::cout << (char)211 << " = {";
	for (int index = 0; index < Sigma.length(); index++)
	{
		std::cout << Sigma[index];
		if (index < Sigma.length() - 1)
			std::cout << ", ";
	}
	std::cout << '}';
	Sleep(T);
	if (Delta.size() > 0)
	{
		std::cout << "\n\n " << (char)228 << ": " << Delta[0].state_1 << " -(" << Delta[0].symbol << ")-> " << Delta[0].state_2;
		Sleep(T);
		for (int index = 1; index < Delta.size(); index++)
		{
			std::cout << "\n    " << Delta[index].state_1 << " -(" << Delta[index].symbol << ")-> " << Delta[index].state_2;
			Sleep(T);
		}
	}
	std::cout << "\n\n q0 = " << q0;
	Sleep(T);
	std::cout << "\n\n F = {\n";
	for (int index = 0; index < F.size(); index++)
	{
		std::cout << "         " << F[index];
		if (index != F.size() - 1)
			std::cout << "\n";
	}
	std::cout << "\n }\n\n";
	Sleep(T);
	SetConsoleTextAttribute(hConsole, white);
}

void DFA::printDFA2()
{
	for (int index = 0; index < Q.size(); index++)
	{
		std::cout << Q[index];
		if (index != Q.size() - 1)
			std::cout << " ";
	}

	std::cout << "\n";
	for (int index = 0; index < Sigma.length(); index++)
	{
		std::cout << Sigma[index];
		if (index < Sigma.length() - 1)
			std::cout << " ";
	}

	if (Delta.size() > 0)
	{

		for (int index = 0; index < Delta.size(); index++)
			std::cout << "\n" << Delta[index].state_1 << " " << Delta[index].symbol << " " << Delta[index].state_2;
	}

	std::cout << "\n" << q0;

	std::cout << "\n";
	for (int index = 0; index < F.size(); index++)
	{
		std::cout << F[index];
		if (index != F.size() - 1)
			std::cout << " ";
	}
}

bool DFA::addState(std::string state)
{
	for (int index = 0; index < Q.size(); index++)
		if (Q[index] == state)
			return 0;
	this->Q.push_back(state);
	return 1;
}

void DFA::addAlphabet(std::string alphabet)
{
	this->Sigma = alphabet;
}

void DFA::addTransition(DFA_transition t)
{
	this->Delta.push_back(t);
}

void DFA::addInitialState(std::string state)
{
	this->q0 = state;
}

void DFA::addFinalState(std::string state)
{
	this->F.push_back(state);
}

void DFA::setQ(std::vector<std::string> stari)
{
	for (int stare = 0; stare < stari.size(); stare++)
		Q.push_back(stari[stare]);
}

void DFA::setSigma(std::string alfabet)
{
	Sigma = alfabet;
}

void DFA::setDelta(std::vector<DFA_transition> reguli)
{
	for (int regula = 0; regula < reguli.size(); regula++)
		Delta.push_back(reguli[regula]);
}

void DFA::setQ0(std::string stareInitiala)
{
	q0 = stareInitiala;
}

void DFA::setF(std::vector<std::string> stariFinale)
{
	for (int stareFinala = 0; stareFinala < stariFinale.size(); stareFinala++)
		F.push_back(stariFinale[stareFinala]);
}

bool DFA::marcare(std::vector<std::vector<int>>& matrice, int i, int j, std::vector<std::string> Q, std::string Sigma, std::vector<DFA_transition> Delta)
{
	std::string stareNoua1;
	std::string stareNoua2;
	int x, y;
	for (int litera = 0; litera < Sigma.size(); litera++)
	{
		if (Sigma[litera] == ' ')
			litera++;
		stareNoua1.clear();
		stareNoua2.clear();
		for (int regula = 0; regula < Delta.size(); regula++)
		{
			x = -1;
			y = -1;
			if (Delta[regula].state_1 == Q[i] && Delta[regula].symbol == Sigma[litera])
			{
				stareNoua1 = Delta[regula].state_2;
			}
			if (Delta[regula].state_1 == Q[j] && Delta[regula].symbol == Sigma[litera])
			{
				stareNoua2 = Delta[regula].state_2;
			}
		}
		if (stareNoua1.size() > 0 && stareNoua2.size() > 0)
		{
			for (int stare = 0; stare < Q.size(); stare++)
			{
				if (Q[stare] == stareNoua1)
					x = stare;
				if (Q[stare] == stareNoua2)
					y = stare;
			}
			if (matrice[x][y] >= 0 && x > y)
			{
				matrice[i][j] = matrice[x][y] + 1;
				return 1;
			}
			else
			{
				if (matrice[y][x] >= 0 && y > x)
				{
					matrice[i][j] = matrice[y][x] + 1;
					return 1;
				}
			}
		}
	}
	return 0;
}

void DFA::minimizeDFA()
{
	HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);

	// Pasul 1: eliminarea starilor inaccesibile
	std::vector<int> accesibilitateStari;
	accesibilitateStari.resize(Q.size());
	for (int stare = 0; stare < accesibilitateStari.size(); stare++)
		accesibilitateStari[stare] = 0;
	std::vector<int> BF;
	for (int stare = 0; stare < Q.size(); stare++)
		if (Q[stare] == q0)
		{
			BF.push_back(stare);
			accesibilitateStari[stare] = 1;
		}
	int index = 0;
	do {

		std::string stareCurenta = Q[BF[index++]];
		for (int regula = 0; regula < Delta.size(); regula++)
			if (Delta[regula].state_1 == stareCurenta)
				for (int stare = 0; stare < Q.size(); stare++)
					if (Q[stare] == Delta[regula].state_2 && accesibilitateStari[stare] == 0)
					{
						BF.push_back(stare);
						accesibilitateStari[stare] = 1;
					}

	} while (index < BF.size());
	SetConsoleTextAttribute(hConsole, 14);
	std::cout << "\n\n Stari inaccesibile: ";
	SetConsoleTextAttribute(hConsole, 12);
	for (int stare = 0; stare < accesibilitateStari.size(); stare++)
		if (accesibilitateStari[stare] == 0)
			std::cout << Q[stare] << " ";
	for (int stare = 0; stare < accesibilitateStari.size(); stare++)
		if (accesibilitateStari[stare] == 0)
		{
			for (int stare2 = 0; stare2 < F.size(); stare2++)//
				if (F[stare2] == Q[stare])
				{
					F.erase(F.begin() + stare2);
					stare2--;
				}//

			Q.erase(Q.begin() + stare);
			accesibilitateStari.erase(accesibilitateStari.begin() + stare);
			stare--;
		}

	SetConsoleTextAttribute(hConsole, 15);

	std::cout << "\n\n";
	system("pause");
	system("CLS");

	// Pasul 2: crearea matricii subdiagonale
	std::vector<std::vector<int>> matrice;
	matrice.resize(Q.size());
	for (int index = 0; index < Q.size(); index++)
		matrice[index].resize(Q.size());
	for (int row = 0; row < matrice.size(); row++)
		for (int col = 0; col < matrice[row].size(); col++)
			matrice[row][col] = -1;

	// Pasul 3: marcarea perechilor stare finala - stare nefinala
	for (int row = 1; row < matrice.size(); row++)
	{
		for (int col = 0; col < matrice[row].size(); col++)
		{
			if (row > col)
			{
				bool stare1Finala = 0;
				bool stare2Finala = 0;
				for (int stare = 0; stare < F.size(); stare++)
				{
					if (Q[col] == F[stare])
						stare1Finala = 1;
					if (Q[row] == F[stare])
						stare2Finala = 1;
				}
				if ((stare1Finala == 0 && stare2Finala == 1) || (stare1Finala == 1 && stare2Finala == 0))
				{
					matrice[row][col] = 0;
				}
			}
		}
	}

	// Pasul 4: marcarea perechilor care duc in perechi marcate
	bool OK;
	do {

		OK = 1;
		for (int row = 1; row < matrice.size(); row++)
		{
			for (int col = 0; col < matrice[row].size(); col++)
			{
				if (row > col&& matrice[row][col] == -1)
				{
					if (marcare(matrice, row, col, Q, Sigma, Delta)) {
						OK = 0;
					}
				}
			}
		}

	} while (OK == 0);

	// Pasul 5: reconstruirea AFD-ului
	std::vector<std::pair<std::string, std::vector<std::string>>> stariNoi;
	// vector de perechi (stare noua, vector de stari vechi)
	std::vector<std::string> stariSelectate;
	int c = 0;
	for (int col = 0; col < matrice.size(); col++)
	{
		if (stariSelectate.size() < Q.size())
		{
			std::pair<std::string, std::vector<std::string>> stareCurenta;
			int gasit = 0;
			for (int index = 0; index < stariSelectate.size(); index++)
				if (stariSelectate[index] == Q[col])
					gasit = 1;
			if (gasit == 0)
			{
				stareCurenta.first = "p";
				stareCurenta.first.append(std::to_string(c));
				c++;
				stareCurenta.second.push_back(Q[col]);
				stariSelectate.push_back(Q[col]);
				for (int row = 1; row < matrice.size(); row++)
				{
					if (row > col&& matrice[row][col] == -1)
					{
						int gasit = 0;
						for (int index = 0; index < stariSelectate.size(); index++)
							if (stariSelectate[index] == Q[row])
								gasit = 1;
						if (gasit == 0)
						{
							stareCurenta.second.push_back(Q[row]);
							stariSelectate.push_back(Q[row]);
						}
					}
				}
				stariNoi.push_back(stareCurenta);
			}
		}
	}

	for (int index = 0; index < stariNoi.size(); index++)
	{
		std::cout << "\n" << stariNoi[index].first << ": ";
		for (int stare = 0; stare < stariNoi[index].second.size(); stare++)
			std::cout << stariNoi[index].second[stare] << " ";
	}

	std::cout << "\n\n";
	system("pause");
	system("CLS");

	DFA DFA2;
	for (int index = 0; index < stariNoi.size(); index++)
		DFA2.Q.push_back(stariNoi[index].first);
	DFA2.Sigma = Sigma;
	int numarReguliAFD2 = 0;
	DFA_transition r;
	for (int regula = 0; regula < Delta.size(); regula++)
	{
		r.symbol = Delta[regula].symbol;
		bool stanga = 0;
		bool dreapta = 0;
		for (int stareNoua = 0; stareNoua < stariNoi.size(); stareNoua++)
		{
			for (int stareVeche = 0; stareVeche < stariNoi[stareNoua].second.size(); stareVeche++)
			{
				if (stariNoi[stareNoua].second[stareVeche] == Delta[regula].state_1)
				{
					r.state_1 = stariNoi[stareNoua].first;
					stanga = 1;
				}
				if (stariNoi[stareNoua].second[stareVeche] == Delta[regula].state_2)
				{
					r.state_2 = stariNoi[stareNoua].first;
					dreapta = 1;
				}
				if (stanga == 1 && dreapta == 1)
				{
					int exista = 0;
					for (int regulaAFD2 = 0; regulaAFD2 < DFA2.Delta.size(); regulaAFD2++)
						if (DFA2.Delta[regulaAFD2].state_1 == r.state_1 &&
							DFA2.Delta[regulaAFD2].symbol == r.symbol &&
							DFA2.Delta[regulaAFD2].state_2 == r.state_2)
							exista = 1;
					if (!exista)
					{
						DFA2.Delta.push_back(r);
						numarReguliAFD2++;
					}
				}
			}
		}
	}

	for (int stareNoua = 0; stareNoua < stariNoi.size(); stareNoua++)
	{
		for (int stareVeche = 0; stareVeche < stariNoi[stareNoua].second.size(); stareVeche++)
		{
			if (stariNoi[stareNoua].second[stareVeche] == q0)
			{
				DFA2.q0 = stariNoi[stareNoua].first;
			}
		}
	}

	for (int stareNoua = 0; stareNoua < stariNoi.size(); stareNoua++)
	{
		for (int stareVeche = 0; stareVeche < stariNoi[stareNoua].second.size(); stareVeche++)
		{
			for (int stareFinala = 0; stareFinala < F.size(); stareFinala++)
			{
				if (stariNoi[stareNoua].second[stareVeche] == F[stareFinala])
				{
					bool exista = 0;
					for (int stareFinalaAFD2 = 0; stareFinalaAFD2 < DFA2.F.size(); stareFinalaAFD2++)
						if (DFA2.F[stareFinalaAFD2] == stariNoi[stareNoua].first)
							exista = 1;
					if (exista == 0)
						DFA2.F.push_back(stariNoi[stareNoua].first);
				}
			}
		}
	}

	DFA2.printDFA();
	SetConsoleTextAttribute(hConsole, 15);

	system("pause");
	system("CLS");
}